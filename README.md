# All U Need Is Pizza

> _Made With **Java** 11_

Isomorphic Projects For Testing Technologies With CRUD Pattern

## File Structure

```
.
+-- src
    +-- main
        +-- java
            +-- xyz.mrdoomy.aunip
                +-- models
                    +-- CreatePayload.java
                    +-- DeletePayload.java
                    +-- Pizza.java
                    +-- UpdatePayload.java
                +-- resolvers
                    +-- Mutation.java
                    +-- Query.java
                +-- services
                    +-- repositories
                        +-- PizzaRepository.java
                    +-- PizzaService.java
                    +-- PizzaServiceImpl.java
                +-- AunipApplication.java
        +-- resources
            +-- data
                +-- pizzas.json
            +-- application.yml
            +-- schema.graphqls
    +-- test
        +-- java
            +-- xyz.mrdoomy.aunip
                +-- models
                    +-- PizzaTest.java
                +-- resolvers
                    +-- MutationTest.java
                    +-- QueryPizza.java
                +-- services
                    +-- PizzaServiceTest.java
+-- .gitignore
+-- build.gradle
+-- gradlew
+-- gradlew.bat
+-- LICENSE
+-- README.md
+-- settings.gradle
```

## Process

Repository:

```
git clone https://gitlab.com/aunip/graphql/spring.git
```

Install:

```
./gradlew
```

Launch:

```
./gradlew bootRun
```

Build:

```
./gradlew build
```

Test:

```
./gradlew test
```

### Requirement

- [x] **MongoDB** Server

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
