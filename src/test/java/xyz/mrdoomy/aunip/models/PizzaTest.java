package xyz.mrdoomy.aunip.models;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
class PizzaTest {

    @Test
    public void testPizza() {
        Set<String> items = new HashSet<>();

        items.add("Choco");
        items.add("Nuts");

        Pizza pizza = new Pizza("PizzaTella", items, 9.99);

        assertEquals("PizzaTella", pizza.getLabel());
        assertEquals(2, pizza.getItems().size());
        assertEquals(9.99, pizza.getPrice());
    }
}