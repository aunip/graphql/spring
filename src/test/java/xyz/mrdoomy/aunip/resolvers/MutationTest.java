package xyz.mrdoomy.aunip.resolvers;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import xyz.mrdoomy.aunip.models.CreatePayload;
import xyz.mrdoomy.aunip.models.DeletePayload;
import xyz.mrdoomy.aunip.models.Pizza;
import xyz.mrdoomy.aunip.models.UpdatePayload;
import xyz.mrdoomy.aunip.services.PizzaService;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@DataMongoTest
@RunWith(MockitoJUnitRunner.class)
class MutationTest {

    @Mock
    PizzaService pizzaService;

    @InjectMocks
    Mutation mutation;

    @Test
    public void testCreatePizza() {
        Set<String> items = new HashSet<>();

        items.add("Choco");
        items.add("Nuts");

        doAnswer(invocation -> {
            Pizza result = invocation.getArgument(0);
            result.setId("42");
            return result;
        }).when(pizzaService).addPizza(any(Pizza.class));

        CreatePayload payload = mutation.createPizza("PizzaTella", items, 9.99);

        assertEquals("42", payload.getCreatedId());

        verify(pizzaService, times(1)).addPizza(any(Pizza.class));
    }

    @Test
    public void testUpdatePizza() {
        Set<String> items = new HashSet<>();

        items.add("Choco");
        items.add("Nuts");

        Pizza currentPizza = new Pizza("PizzaTella", items, 9.99);

        currentPizza.setId("42");

        when(pizzaService.findPizza("42")).thenReturn(Optional.of(currentPizza));
        doNothing().when(pizzaService).updatePizza(any(Pizza.class));

        UpdatePayload payload = mutation.updatePizza("42", Optional.of("PizzaTella"), Optional.of(items), Optional.of(9.99));

        assertEquals(currentPizza.getId(), payload.getUpdatedId());

        verify(pizzaService, times(1)).findPizza("42");
        verify(pizzaService, times(1)).updatePizza(any(Pizza.class));
    }

    @Test
    public void testDeletePizza() {
        doNothing().when(pizzaService).removePizza(anyString());

        DeletePayload payload = mutation.deletePizza("42");

        assertEquals("42", payload.getDeletedId());

        verify(pizzaService, times(1)).removePizza("42");
    }
}