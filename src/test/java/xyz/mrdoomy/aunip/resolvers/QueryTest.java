package xyz.mrdoomy.aunip.resolvers;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import xyz.mrdoomy.aunip.models.Pizza;
import xyz.mrdoomy.aunip.services.PizzaService;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@DataMongoTest
@RunWith(MockitoJUnitRunner.class)
class QueryTest {

    @Mock
    PizzaService pizzaService;

    @InjectMocks
    Query query;

    @Test
    public void testPizzas() {
        List<Pizza> allPizzas = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Set<String> items = new HashSet<>();

            for (int j = 0; j < i + 1; j++) {
                items.add("Item " + j);
            }

            Pizza newPizza = new Pizza("Pizza " + i, items, 9.99 + i);
            newPizza.setId(i + "");
            allPizzas.add(newPizza);
        }

        when(pizzaService.findAllPizzas()).thenReturn(allPizzas);

        Collection<Pizza> results = query.pizzas();

        /*
        results.forEach(pizza -> {
            System.out.println(pizza.toString());
        });
        */

        assertEquals(10, results.size());
    }

    @Test
    public void testPizza() {
        Set<String> items = new HashSet<>();

        items.add("Choco");
        items.add("Nuts");

        Pizza newPizza = new Pizza("PizzaTella", items, 9.99);

        newPizza.setId("42");

        when(pizzaService.findPizza("42")).thenReturn(Optional.of(newPizza));

        Optional<Pizza> result = query.pizza("42");

        Pizza pizza = result.get();

        assertEquals("PizzaTella", pizza.getLabel());
        assertEquals(2, pizza.getItems().size());
        assertEquals(9.99, pizza.getPrice());
    }
}