package xyz.mrdoomy.aunip.services;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import xyz.mrdoomy.aunip.models.CreatePayload;
import xyz.mrdoomy.aunip.models.Pizza;
import xyz.mrdoomy.aunip.models.UpdatePayload;
import xyz.mrdoomy.aunip.services.repositories.PizzaRepository;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@DataMongoTest
@RunWith(MockitoJUnitRunner.class)
class PizzaServiceTest {

    @Mock
    PizzaRepository pizzaRepository;

    @InjectMocks
    PizzaServiceImpl pizzaService;

    @Test
    public void testFindAllPizzas() {
        List<Pizza> allPizzas = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Set<String> items = new HashSet<>();

            for (int j = 0; j < i + 1; j++) {
                items.add("Item " + j);
            }

            Pizza newPizza = new Pizza("Pizza " + i, items, 9.99 + i);
            newPizza.setId(i + "");
            allPizzas.add(newPizza);
        }

        when(pizzaRepository.findAll()).thenReturn(allPizzas);

        Collection<Pizza> results = pizzaService.findAllPizzas();

        /*
        results.forEach(pizza -> {
            System.out.println(pizza.toString());
        });
        */

        assertEquals(10, results.size());
    }

    @Test
    public void testFindPizza() {
        Set<String> items = new HashSet<>();

        items.add("Choco");
        items.add("Nuts");

        Pizza newPizza = new Pizza("PizzaTella", items, 9.99);

        newPizza.setId("42");

        when(pizzaRepository.findById("42")).thenReturn(Optional.of(newPizza));

        Optional<Pizza> result = pizzaService.findPizza("42");

        Pizza pizza = result.get();

        assertEquals("PizzaTella", pizza.getLabel());
        assertEquals(2, pizza.getItems().size());
        assertEquals(9.99, pizza.getPrice());
    }

    @Test
    public void testAddPizza() {
        Set<String> items = new HashSet<>();

        items.add("Choco");
        items.add("Nuts");

        Pizza newPizza = new Pizza("PizzaTella", items, 9.99);

        when(pizzaRepository.save(any(Pizza.class))).thenAnswer(invocation -> {
            Pizza result = invocation.getArgument(0);
            result.setId("42");
            return result;
        });

        pizzaService.addPizza(newPizza);

        assertEquals("42", newPizza.getId());

        verify(pizzaRepository, times(1)).save(any(Pizza.class));
    }

    @Test
    public void testUpdatePizza() {
        Set<String> items = new HashSet<>();

        items.add("Choco");
        items.add("Nuts");

        Pizza currentPizza = new Pizza("PizzaTella", items, 9.99);

        currentPizza.setId("42");

        when(pizzaRepository.save(any(Pizza.class))).thenReturn(currentPizza);

        pizzaService.updatePizza(currentPizza);

        assertEquals("42", currentPizza.getId());
        assertEquals("PizzaTella", currentPizza.getLabel());
        assertEquals(2, currentPizza.getItems().size());
        assertEquals(9.99, currentPizza.getPrice());

        verify(pizzaRepository, times(1)).save(any(Pizza.class));
    }

    @Test
    public void testRemovePizza() {
        doNothing().when(pizzaRepository).deleteById(anyString());

        pizzaService.removePizza("42");

        verify(pizzaRepository, times(1)).deleteById("42");
    }
}