package xyz.mrdoomy.aunip.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import xyz.mrdoomy.aunip.models.Pizza;
import xyz.mrdoomy.aunip.services.repositories.PizzaRepository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class PizzaServiceImpl implements PizzaService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    PizzaRepository repository;

    @Value("classpath:data/pizzas.json")
    private Resource resourceFile;

    @PostConstruct
    private void initPizzas() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<Pizza> pizzas = mapper.readValue(resourceFile.getInputStream(), new TypeReference<List<Pizza>>(){});

            for (Pizza p: pizzas) {
                Optional<Pizza> result = findPizzaByLabel(p.getLabel());

                if (result.isEmpty()) {
                    addPizza(p);
                } else {
                    Pizza pizza = result.get();
                    pizza.setItems(p.getItems());
                    pizza.setPrice(p.getPrice());
                    updatePizza(pizza);
                }
            }
        } catch(IOException e) {
            LOGGER.error("IOException: {}", e);
        }
    }

    @Override
    public Long countPizzas() {
        return repository.count();
    }

    @Override
    public void addManyPizzas(List<Pizza> pizzas) {
        repository.saveAll(pizzas);
    }

    @Override
    public void addPizza(Pizza pizza) {
        repository.save(pizza);
    }

    @Override
    public Collection<Pizza> findAllPizzas() {
        return repository.findAll();
    }

    @Override
    public Optional<Pizza> findPizza(String id) {
        return repository.findById(id);
    }

    @Override
    public Optional<Pizza> findPizzaByLabel(String label) {
        return repository.findByLabel(label);
    }

    @Override
    public void updatePizza(Pizza pizza) {
        repository.save(pizza);
    }

    @Override
    public void removeAllPizzas() {
        repository.deleteAll();
    }

    @Override
    public void removePizza(String id) {
        repository.deleteById(id);
    }

}
