package xyz.mrdoomy.aunip.models;

public class UpdatePayload {

    private String updatedId;

    public UpdatePayload(String updatedId) {
        this.updatedId = updatedId;
    }

    public String getUpdatedId() {
        return updatedId;
    }

    public void setUpdatedId(String updatedId) {
        this.updatedId = updatedId;
    }

    @Override
    public String toString() {
        return "UpdatePayload (UpdatedID: " + updatedId + ")";
    }
}
