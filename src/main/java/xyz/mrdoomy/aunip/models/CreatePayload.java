package xyz.mrdoomy.aunip.models;

public class CreatePayload {

    private String createdId;

    public CreatePayload(String createdId) {
        this.createdId = createdId;
    }

    public String getCreatedId() {
        return createdId;
    }

    public void setCreatedId(String createdId) {
        this.createdId = createdId;
    }

    @Override
    public String toString() {
        return "CreatePayload (CreatedID: " + createdId + ")";
    }
}
