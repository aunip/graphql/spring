package xyz.mrdoomy.aunip.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

@Document(collection = "pizzas")
public class Pizza {

    @Id
    private String id;

    private String label;
    private Set<String> items;
    private Double price;

    public Pizza() {}

    public Pizza(String label, Set<String> items, Double price) {
        this.label = label;
        this.items = items;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Set<String> getItems() {
        return items;
    }

    public void setItems(Set<String> items) {
        this.items = items;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Pizza (ID: " + id + ", Label: " + label + ", Items: " + items + ", Price: " + price + ")";
    }
}
