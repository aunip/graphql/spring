package xyz.mrdoomy.aunip.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.stereotype.Component;
import xyz.mrdoomy.aunip.models.CreatePayload;
import xyz.mrdoomy.aunip.models.DeletePayload;
import xyz.mrdoomy.aunip.models.Pizza;
import xyz.mrdoomy.aunip.models.UpdatePayload;
import xyz.mrdoomy.aunip.services.PizzaService;

import java.util.Optional;
import java.util.Set;

@Component
public class Mutation implements GraphQLMutationResolver {

    private PizzaService pizzaService;

    public Mutation(PizzaService pizzaService) {
        this.pizzaService = pizzaService;
    }

    public CreatePayload createPizza(String label, Set<String> items, Double price) {
        Pizza pizza = new Pizza(label, items, price);
        pizzaService.addPizza(pizza);
        CreatePayload payload = new CreatePayload(pizza.getId());
        return payload;
    }

    public UpdatePayload updatePizza(String id, Optional<String> label, Optional<Set<String>> items, Optional<Double> price) {
        Optional<Pizza> optPizza = pizzaService.findPizza(id);

        if (optPizza.isEmpty()) {
            UpdatePayload payload = new UpdatePayload("");
            return payload;
        }

        Pizza pizza = optPizza.get();

        if (label.isPresent()) {
            pizza.setLabel(label.get());
        }

        if (items.isPresent()) {
            pizza.setItems(items.get());
        }

        if (price.isPresent()) {
            pizza.setPrice(price.get());
        }

        pizzaService.updatePizza(pizza);
        UpdatePayload payload = new UpdatePayload(pizza.getId());
        return payload;
    }

    public DeletePayload deletePizza(String id) {
        pizzaService.removePizza(id);
        DeletePayload payload = new DeletePayload(id);
        return payload;
    }

}
