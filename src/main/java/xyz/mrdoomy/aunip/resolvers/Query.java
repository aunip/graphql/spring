package xyz.mrdoomy.aunip.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.stereotype.Component;
import xyz.mrdoomy.aunip.models.Pizza;
import xyz.mrdoomy.aunip.services.PizzaService;

import java.util.Collection;
import java.util.Optional;

@Component
public class Query implements GraphQLQueryResolver {

    private PizzaService pizzaService;

    public Query(PizzaService pizzaService) {
        this.pizzaService  = pizzaService;
    }

    public Collection<Pizza> pizzas() {
        return pizzaService.findAllPizzas();
    }

    public Optional<Pizza> pizza(String id) {
        return pizzaService.findPizza(id);
    }

}
