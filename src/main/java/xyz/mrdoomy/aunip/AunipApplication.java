package xyz.mrdoomy.aunip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AunipApplication {

	public static void main(String[] args) {
		SpringApplication.run(AunipApplication.class, args);
	}

}
